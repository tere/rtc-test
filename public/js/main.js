navigator.getUserMedia = navigator.getUserMedia || navigator.mozGetUserMedia || navigator.webkitGetUserMedia;
window.RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
window.RTCIceCandidate = window.RTCIceCandidate || window.mozRTCIceCandidate || window.webkitRTCIceCandidate;
window.RTCSessionDescription = window.RTCSessionDescription || window.mozRTCSessionDescription || window.webkitRTCSessionDescription;

let desktop = new RTCPeerConnection(servers);
desktop.onicecandidate = (e) => {
	if (!e.candidate) return;
	console.log(`Candidate desktop: `, e.candidate)

	socket.emit('candidate', {
		name: 'desktop',
		target: 'server',
		candidate: e.candidate
	});
};
const socket = io('localhost:3000');
var streamToSend;
socket
	.on('connect', (data) => {
		// socket.emit('message', 'Hello');
		// console.log(data);
	})
	.on('message', (msg) => {
		console.log(msg);
	})
	.on('answer', (msg) => {
		console.log(msg);
		desktop.setRemoteDescription(msg.description);
	})
	.on('candidate', (candidate) => {
		console.log('Candidate server: ', candidate);
		desktop.addIceCandidate(new RTCIceCandidate(candidate.candidate));
		// desktop.addStream(streamToSend);
	})

navigator.getUserMedia({ audio: false, video: true }, (stream) => {
		console.log("getUserMedia");
		var video = document.querySelector('video');
		video.srcObject = stream;
		streamToSend = stream;
		video.onloadedmetadata = function (e) {
			startWebRTC();
			video.play();
		};
	},
	(err) => {
		console.log("The following error occurred: " + err.name);
	}
)



var servers = {
	iceServers: [{
		// urls: 'stun:stun.l.google.com:19302', // Google's public STUN server
		urls: 'stun:127.0.0.1' // Google's public STUN server
	}]
}

function startWebRTC() {
	console.log(`Start WEBRTC`)
	streamToSend.getTracks().forEach(t => desktop.addTrack(t, streamToSend));

	desktop.createOffer({
        offerToReceiveAudio: 1,
        offerToReceiveVideo: 1
      }).then(offer => {
		// console.log(offer);
		return desktop.setLocalDescription(offer);
	}).then(() => {
		// console.log(desktop.localDescription);
		socket.emit('offer', {
			name: 'desktop',
			target: 'server',
			description: desktop.localDescription
		})
	}).catch(reason => {
		console.log(`Create offer failed: ${reason}`)
	})

}