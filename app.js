const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const WebSocket = require('ws');
const http = require('http');
const socketio = require('socket.io');
const webrtc = require('wrtc');
// const WebRTC = require('webrtc-native');
const app = express();
const rtc = require('rtc-everywhere')();


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


const log = (m) => JSON.stringify(m, null, 4);

const server = http.createServer(app);

const io = socketio();

io.attach(server)



const servers = {
    iceServers: [{
        // urls: 'stun:stun.l.google.com:19302', // Google's public STUN server
        urls: 'stun:127.0.0.1' // Google's public STUN server
    }]
}
// let rtcServer = new rtc.RTCPeerConnection(servers);

let rtcServers = [];
console.log(webrtc)
io.on('connection', (client) => {
	console.log('New client');
	//   const location = url.parse(req.url, true);
	// You might use location.query.access_token to authenticate or share sessions
	// or req.headers.cookie (see http://stackoverflow.com/a/16395220/151312)
	// console.log(`description: ${log(clientDescription)}`);
	// console.log(client);
	client.rtcSession = new webrtc.RTCPeerConnection(servers);

	client.rtcSession.onaddstream = (e) => {
		console.log("Added stream", e);
	}
	client.rtcSession.onicecandidate = (e) => {
		console.log(`onicecandidate Candidate server`)
		if(!e.candidate) return;
		client.emit('candidate', {
			name: 'server',
			target: 'desktop',
			candidate: e.candidate
		})
	}
	client.rtcSession.onaddstream = (e) => {
		console.log('On add stream: ', e)
	}
	client.on('message', function incoming(message) {
		console.log('received: %s', JSON.stringify(message));
		io.emit('message', JSON.stringify({message: "Hello there"}))
	});
	client.on('offer', (clientDescription) => {
		
		client.rtcSession.setRemoteDescription(new webrtc.RTCSessionDescription(clientDescription.description)).then(() => {
			
			client.rtcSession.createAnswer({
				offerToReceiveAudio: 1,
				offerToReceiveVideo: 1
			  }).then(sessionDescription => {
				console.log("Local description")
				client.rtcSession.setLocalDescription(sessionDescription);
				
				client.emit('answer', {
					name: 'server',
					target: 'desktop',
					description: sessionDescription
				})
				
			}).catch(reason => {
				console.log(log(reason));
			})
		})
	})
	client.on('candidate', (candidate) => {
		console.log(`Desktop candidate`)
		client.rtcSession.addIceCandidate(new webrtc.RTCIceCandidate(candidate.candidate));
		// checkStreamFowing(client);
	})
});

server.listen(3000, function listening() {
	console.log('Listening on %d', server.address().port);
});